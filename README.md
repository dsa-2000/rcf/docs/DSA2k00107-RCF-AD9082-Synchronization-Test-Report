## Get the Document!

You can download the latest gitlab CI-built document [here](https://gitlab.com/api/v4/projects/57835303/jobs/artifacts/main/raw/DSA2k00107-RCF-AD9082-Synchronization-Test-Report.pdf?job=build_job)

## Build Requirements

Documentation is built using pandoc and requires the following packages:

- pandoc (tested with 3.2)
- pandoc-citeproc
- latex

## Instructions for generating PDF version of documentation

To build the document locally:

### Print version

``` shell
cd src
./build_print.sh
```
