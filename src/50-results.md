# Results

Five different tests were run.
These were:

1. Measure relative phase from 40 MHz input signal. Repeated 10 times with both boards reset between each trial.
2. Measure relative phase from 850 MHz input signal. Repeated 10 times with both boards reset between each trial.
3. Measure relative phase from 1330 MHz input signal. Repeated 10 times with both boards reset between each trial.
4. Measure relative phase from 1510 MHz input signal. Repeated 100 times with both boards reset between each trial.
5. Measure relative phase from 1510 MHz input signal. Repeated 100 times with one board reset between each trial.

Correlation phases are shown in histograms in Figures \ref{fig-result-40M}, \ref{fig-result-850M}, \ref{fig-result-1330M}, and \ref{fig-result-1510M}.

Correlation phase results are summarised in the table below.

| Frequency (MHz)  | Trials | Boards Reset | Mean (rads) | RMS (rads) | Peak-to-peak (rads) |
|------------------|--------|--------------|-------------|------------|---------------------|
| 40               | 10     | both | 0.036       | 0.00036    | 0.001               |
| 850              | 10     | both | 0.278       | 0.0026     | 0.009               |
| 1330             | 10     | both | 0.377       | 0.0030     | 0.009               |
| 1510             | 100    | both | 0.454       | 0.0058     | 0.027               |
| 1510             | 100    | one  | 0.442       | 0.0061     | 0.030               |

Equivalently, the results can be expressed in picoseconds of delay:

| Frequency (MHz)  | Trials | Boards Reset | Mean (ps) | RMS (ps) | Peak-to-peak (ps) |
|------------------|--------|--------------|-----------|----------|-------------------|
| 40               | 10     | both | 142       | 1.4      | 4.8               |
| 850              | 10     | both | 52        | 0.49     | 1.8               |
| 1330             | 10     | both | 45        | 0.36     | 1.0               |
| 1510             | 100    | both | 47        | 0.61     | 2.9               |
| 1510             | 100    | one  | 47        | 0.64     | 3.2               |

The test firmware also provides the ability to snapshot critical timing signals (see Figure \ref{fig:firmware-dsp}).

Figure \ref{fig:timing-overplot} shows the relative phase of the start-of-multi-frame strobes arriving at the DSP pipeline and the 32 clock-cycle (at 300 MHz) synchronization pulse generated from the external 375 Hz signal. In this plot, all 230 runs have been overplotted to show the small $\pm 1$ cycle of variation.

![\label{fig:timing-overplot}Relative phase of sync signal and start-of-multi-frame strobe, overplotted for all 230 of the trials above.](images/timing_overplot.png){width=70%}


![\label{fig-result-40M}Correlation phase of 40 MHz input, from 10 trials with both boards reset between trials.](images/sync_test_40MHz_0d_10trial.log.png){height=40%}

![\label{fig-result-850M}Correlation phase of 850 MHz input, from 10 trials with both boards reset between trials.](images/sync_test_850MHz_0d_10trial.log.png){height=40%}

![\label{fig-result-1330M}Correlation phase of 1330 MHz input, from 10 trials with both boards reset between trials.](images/sync_test_1330MHz_0d_10trial.log.png){height=40%}

![\label{fig-result-1510M}Correlation phase of 1510 MHz input, from 100 trials with both boards reset between trials.](images/sync_test_1510MHz_0d_100trial.log.png){height=40%}

![\label{fig-result-1510M-onereset}Correlation phase of 1510 MHz input, from 100 trials with one of the two boards reset between trials.](images/sync_test_1510MHz_reboot161.log.png){height=40%}

