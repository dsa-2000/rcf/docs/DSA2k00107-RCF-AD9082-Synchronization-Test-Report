# Test Setup


## Test Process

The test setup uses coherent, phase-matched reference clocks (150 MHz) and synchronization pulses (375 Hz) generated by a reference generator test board (RGTB).
The RGTB contains circuitry identical to that intended for the final Reference Generator [@ts-design], whereby it locks a 150 MHz VCXO to an external 10 MHz reference and divides the 150 MHz to 375 Hz with the ability to synchronize the latter to an external pulse (typically 1 PPS).
The RGTB also creates 4 copies of each signal.

The test setup uses the RGTB to synchronize two separate ADCs, which are then used to synchronously capture a common RF input.
The relative phase of this input can then be analyzed to determine the consistency of the synchronization of the two ADCs.

The test steps are:

1. Connect two ADC boards to a common RF input with length-matched cables.
2. Connect the ADC boards to 150 MHz reference clocks and 375 Hz synchronization pulses, sourced from a reference generator test board (RGTB). Connections are made with length-matched cables.
3. Using prototype control software, synchronize timestamp generation on the two ADC FPGA hosts.
4. Capture a snapshot of ADC sample data from the two boards at a common timestamp.
5. Correlate this data, and record the relative phase of the signals at the two ADC inputs.
6. Repeat steps 3-5, resetting the ADC and FPGA boards between each capture. In four tests, both boards are reset in this step. In one test, only one board is reset.
7. Analyze the consistency of the relative phase between ADC inputs.

## Hardware Test Setup

The RCF hardware used in this test comprises a pair of FPGA + ADC development boards. These are:

- FPGA board: iWave iW-RainboW-G35M Development Kit ([ZU11 SoM](https://www.iwavesystems.com/product/zu19-zu17-zu11-zynq-ultrascale-mpsocsom/))
- ADC board: Analog Devices [AD9082-FMCA-EBZ](https://www.analog.com/en/resources/evaluation-hardware-and-software/evaluation-boards-kits/eval-ad9082.html) (AD9082) FMC+ board

The ADC is hosted on the FPGA board's _FMC+_ connector.

Timing signals are applied to the ADC and FPGA boards using the following connections, chosen so as to emulate the intended DSA2000 production system.
Note that this configuration involves several hardware modifications of the ADC board (schematic: @ad9082-schematic).

1. 150 MHz (0 dBm) reference clock from the TS board VCXO is connected to the `EXT_CLK` SMP port of the ADC card. This requires the following modifications:
	1. Replace the 4-16 GHz balun `T1D` with a lower-frequency compatible device. In this test, Minicircuits TTC1-33W+ was used.
	2. Swap capacitor `C3D` with `C4D`.
	3. Swap capacitor `C5D` with `C6D`.

The balun `T1D` was not easy to remove, and in practice, the required circuit was achieved by using flying leads and an inverted replacement balun, shown in Figure \ref{fig:balun-mod}.

![\label{fig:balun-mod}Balun modification for 150 MHz reference clock.](images/adc_clk_mods.png)

2. 375 Hz, from the TS board (generated by dividing down the VCXO) is DC-coupled differentially to `EXT_SYS_P` and `EXT_SYS_N` SMP connectors on the ADC board. Feeding this clock to the ADC chip requires the following modifications:
	1. Remove capacitor `C1C` and install a short at `C2C`.
	2. Remove capacitor `C3C` and install a short at `C4C`.


3. Further copies of the 375 Hz synchronization signal from the RGTB board are fed directly to the FPGA boards via pin 7 of the `PMOD1` connector.

The TS distribution board itself is fed from a Keysight 33500B series waveform generator, which provides it with a 10 MHz reference and a 1 PPS signal.
Test ADC RF inputs are provided by a Valon 5009 synthesizer, which is also locked to the Keysight 33500B 10 MHz reference.
The Valon synthesizer can be controlled via a USB interface, allowing its output frequency to be remotely reprogrammed.

The two FPGA/ADC assemblies, and Valon synthesizer, are controlled by a single host PC (`maze`) running Ubuntu 16.04.

An overview of the physical test setup is shown in Figure \ref{fig:test-setup}.

![\label{fig:test-setup}The test setup comprises two ADC-FPGA board assemblies, fed with identical copies of timing signals from a prototype TS timing distribution board.](diagrams/setup.drawio.png){width=100%}

## Software Test Setup

The behaviour of the test system depends upon the firmware running on the FPGA boards, the Linux drivers used to configure the ADC chips, and the high-level control software running on the host PC.

In these tests, the following software versions and configurations were used:

### FPGA test firmware:
- Firmware library: https://gitlab.com/dsa-2000/rcf/fsm/software/src/dsa2000-fpga
- commit `e4e5eb33` (tag `sync-test`)
- Test model: `firmware/src/models/dsa2k_iwave_adc_only_sync_test.slx`

### Firmware libraries
1. Analog Devices JESD Library:
	- https://github.com/analogdevicesinc/hdl
	- commit `f6483036` (sub-moduled in `dsa2000-fpga`)
2. mlib_devel:
	- https://github.com/realtimeradio/mlib_devel
	- commit `0c85150a` (sub-moduled in `dsa2000-fpga`)

### Linux libraries

[ADI-provided Linux libraries](https://wiki.analog.com/resources/tools-software/linux-drivers/iio-mxfe/ad9081) configure the ADC chip on the FMC board and set PLL parameters which determine the ADC sampling rate.

The following libraries and configuration were used:

1. ADI Linux Yocto meta-layer:
	- https://gitlab.com/dsa-2000/rcf/fsm/software/libs/meta-adi
	- commit `fdbd9df4` (sub-moduled in `dsa2000-fpga`)
2. Device tree `dsa2000-fpga/petalinux_adi/system-user.dtsi`

These configure the ADCs to accept an input reference clock of 150 MHz, and to generate a sampling clock of 4800 MHz.

### Startup Behaviour

On startup, the FPGA boards are configured to:

1. Run `tcpborphserver3`, which listens on TCP port 7147 for commands.
	- Available from https://github.com/realtimeradio/katcp_devel
	- commit `95b6223e`
	- build with `SYSROOT=/ make`

2. Program the iWave development board's second SI5341 clock generator to deliver a 300 MHz reference clock to the FPGA's GTY transceivers.
	- Programming script and configuration available in the `dsa2000-fpga` repository at `petalinux_adi/soc_scripts/i2c-scripts`:
		- Programming script: `program_i2c.py`
		- Configuration file: `G35D-SI5341-CS2-REL0.1-SI5341B-clk2-300mhz-Registers.txt`

3. Reset the AD9082 ADC chip and JESD interface following programming of the GTY reference clock. The full start script executed by the FPGA board's CPU on power up is:

	```bash
	# Turn off ADC
	# Enable Silabs PLL
	# Turn on ADC
	echo 'Powering down ADC'
	echo 1 > /sys/bus/iio/devices/iio:device2/powerdown
	echo 'Programming Silabs Synthesizer'
	cd /home/casper/src/dsa2000-fpga/petalinux_adi/soc_scripts/i2c-scripts
	program_i2c.py G35D-SI5341-CS2-REL0.1-SI5341B-clk2-300mhz-Registers.txt
	sleep 0.1
	echo 'Powering up ADC'
	echo 0 > /sys/bus/iio/devices/iio:device2/powerdown
	```

### Software Test Script

The software script used to run the tests described in this document is `test_sync_repeatability.sh`, available in the `dsa2000-fpga` repository at `software/scripts/sync_tests`.

Executed as:

```bash
./test_sync_repeatability.sh <Ntrials> <host1> <host2> <sync_delay>
```

This script will measure correlation phase `Ntrials` times, targetting FPGA boards with IP addresses `host1` and `host2`.
The programmable delay applied to the synchronization signal is `sync_delay` clock cycles (at 300 MHz), as described in \ref{sec:dsp-chain}.

## Firmware Configuration

The firmware is fully specified in the `dsa2k_iwave_adc_only_sync_test.slx` model, which may be compiled using the CASPER/JASPER toolchain.

Broadly, the firmware has two separate parts.
The first part is a JESD interface, and is largely constructed from [firmware provided by Analog Devices](https://github.com/analogdevicesinc/hdl).
The second part is a simple DSP chain, which timestamps and captures incoming data.
This part of the firmware is constructed using [CASPER's](https://casper.berkeley.edu) MATLAB Simulink and Xilinx System Generator tools.

### The JESD Interface

A block diagram showing the JESD interface is shown in Figure \ref{fig:firmware-jesd}.

The main inputs to the interface are:

- 8 high-speed transceiver lanes from the ADC, which operate at a line rate of 19.8 Gb/s. (2 inputs, 16-bit samples, 4.8 GHz sampling rate, 66b/64 encoding, 8 lanes)
- An asynchronous 300 MHz reference clock.
- The 375 Hz synchronization signal, which the ADC uses as a JESD204C SYSREF, and the firmware uses to generate a higher speed internal 2.34375 MHz pulse which the JESD204C link-layer decoder uses as its SYSREF input.

From these inputs, a 300 MHz clock is recovered from the JESD204C data stream, and used to clock most of the internals of the interface.
This clock is synchronous with the 4800 MHz sampling clock.

The 375 Hz input signal is captured onto this clock domain, debounced and delayed by demanding that 4 successive samples (captured at 300 MHz) of the sync signal be high for the internal 2.34375 MHz pulse generation logic to reset. 

The ADI-supplied HDL handles alignment of multiple JESD204C lanes, and uses the 2.34375 MHz SYSREF signal to start releasing valid data only at the start of a JESD204C multi-frame edge coincident with a SYSREF edge.
As such, the first valid sample released by the JESD interface can be considered to be coincident with a 9.375 MHz LMFC edge, and the LMFC signal can be reconstructed by counting samples.


![\label{fig:firmware-jesd}A high-level block diagram of the JESD204C receiver firmware. The interface recovers a 300 MHz clock (which is coherent with the 4800 MHz sampling clock) and presents this to the downstream pipeline with multiframe boundaries -- which occur at a rate of 9.375 MHz -- tagged by a local multiframe clock (LMFC) strobe.](diagrams/firmware-config-jesd.drawio.png){width=100%}

The JESD204C data, as well as 9.375 MHz LMFC and 375 Hz SYSREF strobes, are passed to the DSP chain via a FIFO.
This FIFO permits the downstream processing logic to run at a different (potentially asynchronous) clock rate to the JESD interface.
However, in these tests the DSP chain runs at the same 300 MHz rate as the JESD interface.


### The DSP Chain \label{sec:dsp-chain}

The DSP chain receives deformatted ADC samples, and captures a burst of them starting from a known timestamp.
A high-level block diagram of the DSP chain is shown in Figure \ref{fig:firmware-dsp}.

Configuring the DSP chain to trigger sample capture involves the following steps:

1. Picking a delay which, when applied to the 375 Hz synchronization signal, reliably places an LMFC positive edge (i.e. start-of-multiframe marker) in the center of a window defined by the 32 300 MHz clock cycles (corresponding to 512 ADC samples) following a 375 Hz pulse.
An appropriate delay is chosen by examining timing plots of the type shown in Figure \ref{fig:timing-overplot} and a single delay is used for all boards and all tests.
2. Triggering -- via a software command -- an internal timestamp counter to reset to a known value on the next LMFC edge coincident with a synchronization pulse.
3. Capturing 32768 ADC samples at the moment the timestamp counter reaches a known value.

Data capture on two boards is synchronized by making sure that the timestamp counter on both boards loads on the same LMFC edge.
The load procedure is:
1. Wait for a synchronization pulse (i.e. LMFC pulse coincident with a 375 Hz pulse) to pass on one of the boards.
2. Configure the timestamp counters on both boards so that they will load an appropriate value on the next synchronization pulse.
3. Check that, having configured the counter for load, a synchronization pulse has not yet arrived on the first board.
4. If a pulse had arrived, retry the whole procedure, since it cannot be guaranteed that the synchronization pulse did not arrive prior to completion of configuration.

![\label{fig:firmware-dsp}A high level block diagram of the simple DSP pipeline which processes data from the JESD204C receiver. Since the JESD204C receiver logic locks frame boundaries (indicated by LMFC strobes) to the 375 Hz SYSREF with consistent phase, the job of the timestamping logic is to associate a known timestamp with a particular SYSREF edge. Once the relationship between one SYSREF edge and a timestamp is defined, further timestamps can be generated by counting samples.](diagrams/firmware-config-dsp.drawio.png){width=100%}