#!/bin/bash

FNAME=DSA2k00107-RCF-AD9082-Synchronization-Test-Report.pdf

#cd diagrams
#./exportpng.sh
#cd ..

pandoc *.md -f markdown+pipe_tables -o $FNAME -s \
 -V colorlinks -V links-as-notes \
 -V logo=doc-templates/pandoc/d2k-logo.png --number-sections \
 --template doc-templates/pandoc/template.latex \
 --citeproc

