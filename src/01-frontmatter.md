---
title: AD9082 Synchronization Test Report
author:
- Jack Hickish\textsuperscript{1}
affiliation:
- \textsuperscript{1} Real-Time Radio Systems Ltd
abstract:
bibliography: bibliography.bib
document-number: 00107
wbs-level-2-abbrev: RCF
document-type-abbrev: TST
revisions:
- version: 1
  date: 2024-05-13
  remarks: Original
  authors:
  - JH
- version: 1.0.1
  date: 2024-06-12
  remarks: Reorganise data. No documentation changes
  authors:
  - JH
...
