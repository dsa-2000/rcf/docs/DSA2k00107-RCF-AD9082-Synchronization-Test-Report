# Introduction {#sec-introduction}

The DSA-2000 telescope will digitize several thousand analog RF signals, and uses interferometric techniques to combine the resultant digitized signals to form images of the sky.
Image making (or, similarly, beamforming) requires that differences in arrival time of wavefronts at each of the DSA antennas be known to high precision.
This typically places two requirements on the digitization system:

1. Digitization must be coherent across all channels.
2. Digital samples for each data stream must be timestamped in such a way that a sample can be traced to an arrival time at an antenna.

The first of these requirements is most easily met by distributing a common reference clock to all digitizers in the system and multiplying this clock up to the desired sampling rate.
The second requirement is typically met by distributing some variety of synchronization pulse to all digitizers in the system, and using this to tag the first sample following a pulse with a common timestamp across the system.
Having tagged this single sample, future samples can be timestamped by counting samples from the first.

The process of turning a sample's timestamp into an arrival time at an antenna - expressed in an astronomical time standard - is then a matter of knowing:

1. The arrival time of the synchronization pulse, expressed in the time standard of interest.
2. The duration of time between the synchronization pulse and the sample in question.
3. The delay incurred by the analog signal path between the antenna and the digitizer.

The first - timing of the sync pulse - is managed by the DSA's Timing and Synchronization (TS) system, described in detail in @ts-design.
The second - the time between the sync pulse and the sample - is known, provided that the TS system lock the digitizer's sampling reference clock to an appropriate time standard.
The last - the delay between the antenna and the digitizer - depends on the analog cabling and electronics, and is time dependent due to temperature and other environmental factors. This delay must be periodically measured and corrected for by calibrating the array using astronomical sources.
A more detailed discussion of tracing of timestamps to wavefront arrival times can be found in @ts-accounting-for-delays.

In order that the only uncertainty in converting a timestamp to an arrival time at the antenna be slow environmental changes, it is important that the RCF and TS subsystems do not introduce any non-deterministic delays.
Specifically, no uncertainties in the timestamping mechanism should be introduces by power cycling digital hardware.
Modern high-speed digitizers (and their interfaces to downstream signal processing devices) are specifically designed to support such determinism (see @adi-jesd204b-sync and @adi-jesd204c-sync for discussion of "Deterministic Latency" in the context of the JESD204B and JESD204C standards) and DSA relies on these features.

## Test Objectives

We wish to assess if the hardware planned to be used by the DSA RCF and TS subsystems can generate deterministic timestamps of input data.
Specifically, we wish to verify that the timestamping mechanism does not introduce any non-deterministic delays when the digitizer is reset and reconfigured.
The tests described in this document are designed to most closely replicate the implementation of the timing distribution system planned for the production DSA-2000 telescope, shown in Figure \ref{fig:ts-config}.

![\label{fig:ts-config}The planned TS timing and distribution system involves distributing a reference clock (at 150 MHz) and a synchronization signal (at 375 Hz) to all ADC and FPGA boards in the telescope. _Figure Source: ADC Clocking Options Powerpoint Slides, D'Addario (2024)_](images/ts-architecture.png){width=100%}
