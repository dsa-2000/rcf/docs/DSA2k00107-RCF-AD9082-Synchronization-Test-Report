# Conclusions

A test setup has been used to demonstrate synchronized timestamping of data between two ADC-FPGA boards.

The synchronization methodology closely follows that planned for the final DSA2000 system.

Over many test trials (100 with a 1510 MHz test signal, and 10 with 40, 850, and 1330 MHz test signals) in which signals from the two boards were correlated, no timestamping inconsistencies have been observed.
Between each trial ADCs were soft power-cycled and the FPGA fabric was reprogrammed.
We conclude that the planned synchronization methodology is robust against startup inconsistencies.
