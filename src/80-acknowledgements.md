# Acknowledgements

The firmware and software used in these tests make extensive use of the [JESD204C software libraries and firmware cores](https://wiki.analog.com/resources/fpga/peripherals/jesd204) provided by Analog Devices, Inc.
