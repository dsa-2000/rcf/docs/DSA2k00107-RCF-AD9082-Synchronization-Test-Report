#!/usr/bin/env python3

import sys
import pickle as pkl
from matplotlib import pyplot as plt

LABELS = ['LMFC posedge', 'sync pulse', 'sync edge']
STYLE = ['ro-', 'go-', 'bo-']
XMIN = 63
XMAX = 138
FNAME = 'timing_overplot.png'

def main(files):
    plt.figure(figsize=(12, 6))
    for fn, file in enumerate(files):
        with open(file, 'rb') as fh:
            x = pkl.load(fh)
            if fn == 0:
                hostnames = sorted(x.keys())
                plt.suptitle(f'{hostnames[0]} (top), {hostnames[1]} (bottom)')
            for hn, h in enumerate(hostnames):
                plt.subplot(2,1,1+hn)
                for i in range(3):
                    plt.plot((x[h][XMIN:XMAX] >> i) & 1, STYLE[i], label=LABELS[i])
                if fn == 0:
                    plt.ylabel('Logical value')
                    if hn == 1:
                        plt.xlabel('DSP clock count')
                    plt.legend()
    plt.savefig(FNAME)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print('usage: script.py <file1> [<file2> ...]')
        exit()

    main(sys.argv[1:])
    #plt.show()

