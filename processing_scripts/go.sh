#!/bin/bash

./overplot_timing.py ../data/no_ddc/*.pkl
./histogram_phases.py ../data/no_ddc/*.log
mv ./*.png ../src/images
