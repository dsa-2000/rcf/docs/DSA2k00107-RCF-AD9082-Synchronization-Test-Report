#!/usr/bin/env python3

import os
import sys
import re
import numpy as np
from matplotlib import pyplot as plt

SPLITTER_OFFSET = 0 # Phase offset of splitter between RF inputs
SFREQ_MHZ = 4800 # Sampling frequency in MHz

def get_number(text): # Thanks, chatgpt 
    # Use regular expression to find all occurrences of numbers
    # This regex matches optional leading '-' for negative numbers and then digits possibly followed by a decimal point and more digits
    numbers = re.findall(r'-?\d+\.?\d*', text)
    
    # Check if we found any numbers and get the first one
    if numbers:
        first_number = float(numbers[0])  # Convert the first found number to float
        return first_number
    else:
        print("No numbers found")

def main(files):
    for file in files:
        plt.figure(figsize=(6,7))
        freq_mhz = get_number(file)
        
        phases = []
        with open(file, 'r') as fh:
            for line in fh.readlines():
                if not line.startswith('Correlation phase'):
                    continue
                phase = get_number(line)
                if phase is not None:
                    phases += [phase]
        phases = np.array(phases) + SPLITTER_OFFSET
        phases[phases > (2*np.pi)] -= 2*np.pi
        delays_ps = phases / (2 * np.pi * freq_mhz*1e6) * 1e12

        ntrial = len(phases)
        phase_mean = np.mean(phases)
        phase_rms = np.sqrt(np.var(phases))
        phase_p2p = np.max(phases) - np.min(phases)
        delay_mean_ps = np.mean(delays_ps)
        delay_rms_ps = np.sqrt(np.var(delays_ps))
        delay_p2p_ps = np.max(delays_ps) - np.min(delays_ps)

        plt.hist(phases)
        plt.ylabel('Number of occurrences')
        plt.xlabel('Correlation phase [radians]')
        title = f'Sampling frequency: {SFREQ_MHZ} MHz; ' + \
            f'Input frequency {freq_mhz} MHz; ' + \
            f'N: {ntrial}\n' + \
            f'mean: {phase_mean:.3f} ({delay_mean_ps:.3f} ps)\n' + \
            f'rms: {phase_rms:.1e} ({delay_rms_ps:.1e} ps)\n' + \
            f'peak-to-peak: {phase_p2p:.3f} ({delay_p2p_ps:.1f} ps)'
        plt.title(title)
        plt.savefig(os.path.basename(file) + '.png')

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print('usage: script.py <file1> [<file2> ...]')
        exit()

    main(sys.argv[1:])
    #plt.show()



